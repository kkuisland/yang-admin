import { RouteRecordRaw } from 'vue-router'

const routes: RouteRecordRaw[] = [
	{
		path: '/',
		component: () => import('layouts/MainLayout.vue'),
		children: [{ path: '', component: () => import('/src/modules/common/IndexPage.vue') }],
	},

	{
		path: '/yang',
		component: () => import('layouts/MainLayout.vue'),
		children: [
			{ path: 'farm', component: () => import('/src/modules/farm/IndexPage.vue') },
			{ path: 'util', component: () => import('/src/modules/util/IndexPage.vue') },
		],
	},
	// Always leave this as last one,
	// but you can also remove it
	{
		path: '/:catchAll(.*)*',
		component: () => import('/src/modules/error/ErrorNotFound.vue'),
	},
]

export default routes
